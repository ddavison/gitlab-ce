module QA
  module Factory
    module Resource
      class ProjectMilestone < Factory::Base
        attr_accessor :description

        dependency Factory::Resource::Project, as: :project

        def title=(title)
          @title = "#{title}-#{SecureRandom.hex(4)}"
          @description = 'A milestone'
        end

        product :title do
          Page::Project::Milestone::Show.act { milestone_title }
        end

        def fabricate!
          Runtime::Browser.visit(:gitlab, Page::Dashboard::Milestones)

          Page::Dashboard::Milestones.perform do |milestone_page|
            milestone_page.select_project_or_group(@project.name)
          end

          Page::Project::Milestone::New.perform do |milestone_new|
            milestone_new.set_title(@title)
            milestone_new.set_description(@description)
            milestone_new.create_new_milestone
          end
        end
      end
    end
  end
end
