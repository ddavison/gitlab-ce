module QA
  feature 'creates a merge request', :core do
    scenario 'user creates a new merge request'  do
      Runtime::Browser.visit(:gitlab, Page::Main::Login)
      Page::Main::Login.act { sign_in_using_credentials }

      Factory::Resource::MergeRequest.fabricate! do |merge_request|
        merge_request.title = 'This is a merge request'
        merge_request.description = 'Great feature'
      end

      expect(page).to have_content('This is a merge request')
      expect(page).to have_content('Great feature')
      expect(page).to have_content('Opened less than a minute ago')
    end

    scenario 'user attaches a milestone to the merge request' do
      Runtime::Browser.visit(:gitlab, Page::Main::Login)
      Page::Main::Login.act { sign_in_using_credentials }

      current_project = Factory::Resource::Project.fabricate! do |project|
        project.name = 'project-with-merge-request-and-milestone'
      end

      current_milestone = Factory::Resource::ProjectMilestone.fabricate! do |milestone|
        milestone.title = 'unique-milestone'
        milestone.project = current_project
      end


      Factory::Resource::MergeRequest.fabricate! do |merge_request|
        merge_request.title = 'This is a merge request with a milestone'
        merge_request.description = 'Great feature with milestone'
        merge_request.project = current_project

        # merge_request.assignee =
        merge_request.milestone = current_milestone
        # merge_request.labels = %w()
      end

      expect(page).to have_content('This is a merge request with a milestone')
      expect(page).to have_content('Great feature with milestone')
      expect(page).to have_content('Opened less than a minute ago')
      expect(page).to have_content(current_milestone.title)
    end
  end
end
