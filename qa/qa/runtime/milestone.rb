module QA
  module Runtime
    module Milestone
      extend self

      def default_title
        'QA-Milestone'
      end

      def title
        Runtime::Env.milestone_title || default_title
      end

      def description
        Runtime::Env.milestone_description || 'This is a test milestone'
      end
    end
  end
end
