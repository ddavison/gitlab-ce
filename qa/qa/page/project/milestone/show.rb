module QA
  module Page
    module Project
      module Milestone
        class Show < Page::Base
          def milestone_title
            find('h2.title').text
          end
        end
      end
    end
  end
end

