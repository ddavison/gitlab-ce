module QA
  module Page
    module Dashboard
      class Milestones < Page::Base
        view 'app/views/dashboard/milestone/index.html.haml'

        def select_project_or_group(project_or_group)
          find('.new-project-item-select-button').click

          find('ul.select2-results > li', text: project_or_group).click
          find('.new-project-item-link').click
        end

        def self.path
          '/dashboard/milestones'
        end
      end
    end
  end
end
